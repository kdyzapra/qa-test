# QA-Test   
##Find a way to automate the following tests (Never modify the url to access a specific place unless it is the first step)
* You can automate the following any way you want but to get you started, there's a sample script provided in the folder
* `npm i` to install the required node modules
* `npm run protractor` to run the test

## Test 1
* Start at "https://www.google.com/"
* By searching through Google's search engine and clicking on links, fin a way to get to "https://www.youtube.com/"
* Verify that user have landed on YouTube, everything should fail if user has not landed on the correct site

## Test 2
* Start at "https://www.youtube.com/"
* Click on the first video that appears on the top left (First row, first column; not the advertisement)
* Once user has landed on the selected video, click on the first video that appears in the "Up Next" column (The top most video from the column that appears on the right)
* Once user has landed on the selected video, click on the YouTube icon
* Verify that user have landed on YouTube, everything should fail if user has not landed on the correct site

## Test 3
* Start at "https://automation.dev.coinspectapp.com/"
* Login using the following credentials:
	* username: qa_technicaltest
	* password: QWERTYqwerty123!@#
* Start an inspection utilizing the following combination
	* property: Property for Automation A
	* checklist: Automated Color Test
* Answer everything randomly (add random comments, select random options or items from drop-downs, etc)
* Find a way to upload a random number of photo for every question (ranging from 1-3)
* For every photo uploaded, add a random comment (Not shown in the video, but a pencil-like icon should appear below the picture uploaded)
* Mark the inspection as done
* After marking the inspection as done, generate a report (Not shown in the video)
	* After clicking on the mark as done button, the "Create Report" button will apear
	* After clicking on the "Create Report" button, the "Download Report" button will apear
	* Click on the "Download Report" button
* A new tab will open and try to render a PDF, verify that the PDF is rendered
* Go back to the homepage
* Verify that the "DONE" count has increased
* Logout


